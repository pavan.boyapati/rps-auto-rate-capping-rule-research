
# RPS Auto Rate Capping Rule Research

Learn more about RPS codebase at https://gitlab.com/amfament/amfam/RATING/rps


---
**Table of Contents**  

- [Scope and Requirements](#scope-and-requirements)
- [Steps to Complete This Research](#steps-to-complete-this-Research)
- [Git Commands](#Git-Commands)
- [Contacts](#contacts)

## What is the capping rules?
- Is a process when credit is reordered
- It will minimize how much your credit factor will get worse
- It will only allow the CPG to change a certain amount so the premium doesn’t  increase too much at once at a credit event
-	Example: (higher number is a worse score)
    -	Current score is 5
    - New score is 15
    - If limitation only allows a movement of 5, the new score would be 10

## Scope and Requirements
1. Auto rate capping rules(Credit Working Session meetings)
   - Checkout the RPS codebase, and understanding the code structure/logic. Specically we need to research with `auto rate capping rules`,and verify/capture the how this capping rules are implementated.
   - Create the documentation for capping rules logic and structure. So that we can validate/implement this auto capping rules in PolicyCenter instance.
1. Auto Vehicle Symbols (being worked on by the PC Team)
   - Auto vehicle related work get implemented in PC instance.  
1. `Review for CPG (Customer Pricing Group) capping rules`
   - May be rules for a calculated CPG, previously applied CPG
   - State specific rules
   - Need IL/KS for sure, but we could use all states if not difficult to document
   - Jason S will bring up prior SMD document that may have helpful information (it was attached in Teams)
1. `How is RPS getting the CPG information? `
    - It is looked up from the rate table and converted from the CBIS (Credit Bureau Insurance Score)
    - CBIS is the score we get from the credit report, it is converted to CPG for rating
    - Is there any information about CBIS?
    - Capping was removed from MO for Auto and Property in 2020
    - Is this supported in the code?

## Additional Requirements
 - Further review of all states
    - E.g. Washington specific rules about life event exceptions and removal of capping

## Steps to Complete This Research
  - Git Access
  - Checkout/clone the git repo
  - Understanding the requirements
  - Research with auto capping rules
  - Create the documentation what you find 

## Git Commands

`Make sure that you have SSH key generated and added in your gitlab account or personal access token.`

```
git clone git@gitlab.com:amfament/amfam/RATING/rps.git
git status
git add .
git commit -am 'Update: read me'
git push
```
## Contacts
  - Brauer, Todd J <TBRAUER@amfam.com>
  - Murphy, Seamus E <SMURPHY@amfam.com>
