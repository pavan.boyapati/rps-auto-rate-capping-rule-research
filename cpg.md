
## Rules
``` java
/**
	 * Method aggregate gets the Prior rating from the DAtaBase and checks for the ProcessCode
	 * @param rating Rating
	 * @return Document
	 * @throws RateProcessingSvcException
	 * @see com.amfam.rateprocsvc.business.RateDataAggregator#aggregate(Rating)
	 */
	public Rating aggregate(Rating rating)
			throws RateProcessingSvcException {

		BaseXml priorRateXml = null;
		boolean includePreviousRatingReport = false;
		
		RateProcessCode rateProcessCode = RateProcessCode.valueOf(rating.getProcessParamValue( 
				RuleProcessor.RATE_PROCESS_CODE_PARAM_NAME));

		priorRateXml = this.getProirRatingFromDB(rating, rateProcessCode);
		
		//Do validation check for prior data.
		this.validatePriorData(priorRateXml, rating);
		/*
		 * If the rate process code is PCSL, then map using AddCPGElementsForCSL.xsl and
		 * return the rating record
		 */
		if(RateProcessCode.PCSL.equals(rateProcessCode)){
			return this.addCPGForCSL(rating, priorRateXml);
		}
		
		
		DataValue creditScoringEvent = getDataValue(CREDIT_SCORING_EVNT_PARAM_NAME,
				rating.getProcessParamValue(CREDIT_SCORING_EVNT_PARAM_NAME), BOOLEAN_DATA_TYPE);
		
		//OR Credit Consent - get process parm value for rule
		String useOfPreviousCreditBasedOnConsent = rating.getProcessParamValue(RuleProcessor.USE_OF_PREVIOUS_CREDIT_BASED_ON_CONSENT);

		/*
		 * Evaluate AA standard and add old score if needed
		 */
		if( (RPSConstants.INCREASED_AA_STANDARD.equals(rating.getProcessParamValue(
				RuleProcessor.CREDIT_AA_STD_PARAM_NAME))) || (rating.getProcessRuleResult(
						RuleProcessor.BEST_PREMIUM_FOR_CREDIT_RULE_NAME)) ) 
		{

			boolean usePrevCBIS = this.usePreviousCBIS(rating, priorRateXml);
			
			// get the old CBIS score
			String prevCBIS = this.getPrevCBIS(priorRateXml, usePrevCBIS);
			rating.setProcessParam(RuleProcessor.PREV_CBIS_PARAM_NAME, prevCBIS);
			String prevScoreType = this.getPrevCBISScoreType(priorRateXml, usePrevCBIS);
			rating.setProcessParam(RuleProcessor.PREV_CBIS_SCORE_TYPE_PARAM_NAME, prevScoreType);

			if(log.isDebugEnabled()){
				log.debug("Adding Prev CBIS: " + prevCBIS +" and previous score type: " + prevScoreType);
			}

			//if Increased_AA_Standard is "I" then check boolean for isReconsiderationEffectiveForState
			includePreviousRatingReport = 
				(rating.getProcessRuleResult(RuleProcessor.BEST_PREMIUM_FOR_CREDIT_RULE_NAME) ||
				rating.getProcessRuleResult(RuleProcessor.BEST_CPG_FOR_PREMIUM_RULE_NAME));
			
		}
		//OR Credit Consent - if useOfPreviousCreditBasedOnConsent is true call the addPriorReport method passing a value of false for the
		//boolean value that determines if the report needs to be added as a previous rating report 
		if("true".equals(useOfPreviousCreditBasedOnConsent)) {
			this.addPriorReport(rating, priorRateXml, AutoPriorRateAggregator.AGGREGATE_PRIOR_REPORT,false);			
            return rating;
		}
				
		if(!rating.getProcessRuleResult(RuleProcessor.CREDIT_PRESCREEN_NOT_APPLICABLE)){		
			switch(rateProcessCode)
			{		
				case BILL:	
				{
/*					
					if (!creditScoringEvent.equals(Boolean.TRUE)) {
						break;
					}
					//If apply CPG capping request get out of switch.
					if ((rating.getProcessRuleResult(APPLY_CPG_CAPPING))) {
						break;
					}
*/
					//Since we don't know if the BILL is a CSE or a NCSE, get prior CPGs.
					includePreviousRatingReport = true;
					break;
				}
				case PCBR:
				case RCBR:
				case CBRRNCK:
				{
					return this.addPriorReport(rating, priorRateXml, 
							AutoPriorRateAggregator.AGGREGATE_PRESCREEN_REPORT, includePreviousRatingReport);
				}
			}
		}
				
		//If apply CPG capping request get old capping values.
		if ( (rating.getProcessRuleResult(APPLY_CPG_CAPPING)) || (rating.getProcessRuleResult(RuleProcessor.IS_NI_COMPARE_CPG_REQUEST_RULE_NAME)) ){
			includePreviousRatingReport = true;			
		}
 		
		//only if credit scoring event is false
		if(creditScoringEvent.equals(Boolean.FALSE)) {
			this.addPriorCPG(rating, priorRateXml);
		//if consent for credit is false	
		}else if (includePreviousRatingReport){
			// This will get called if it is a credit scoring event. A MCHG or CERW or STAT CSE will get invoked only if 
			// increased AA std is 'I'
			this.addPriorReport(rating, priorRateXml, 
					AutoPriorRateAggregator.AGGREGATE_PRIOR_REPORT,true);
		}
	
		return rating;
	}
```
